<h2>Addslashes</h2>
<hr/>
<?php
$string = "Is your name 0'Reilly?";
echo addslashes($string);
?>
<h2>Explode</h2>
<hr/>
<?php
$string = "html css php mysql";
$explodeString = explode(" ",$string);
echo "<pre>";
print_r($explodeString);
echo "</pre>";
echo "<br/>";
?>
<h2>Implode</h2>
<hr/>
<?php
$string = array("html","css","php","mysql");
$implodeString = implode('", "',$string);
echo '"'.$implodeString.'"';
echo "<br/>";
?>
<h2>htmlentities</h2>
<hr/>
<?php
$string = "<br> is a html tag";
echo htmlentities($string);
?>
<h2>Trim</h2>
<hr/>
<?php
$username=" Rahim ";
echo $username;
echo "<br/>";
echo trim($username);
echo "<br/>";
$myString = "line1\nLine2\nLine3\n";
echo nl2br($myString);
echo "<br/>";
$myString = "Hello World! How is Life?";
echo str_pad($myString,40,"#**");
echo "<br/>";
echo str_repeat($myString,3);
echo "<br/>";
echo str_replace('World','Dip',$myString);
echo "<br/>";
print_r(str_split($myString,5));
echo "<br/>";
//substr_compare
$mainStr = "Hello World! How is life?";
$str = "How";
echo substr_compare($mainStr,$str,13);
echo "<br/>";
echo substr_count($mainStr,"l");
echo "<br/>";
echo substr_replace($mainStr,"How are you doing?",6);
echo "<br/>";
$mainStr = "hello world! how is life?";
echo ucfirst($mainStr);
echo "<br/>";
echo ucwords($mainStr);
?>