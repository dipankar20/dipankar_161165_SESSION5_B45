<?php
$var = array(1,2,3,4,5,90);
$float_value_of_var = floatval($var);
echo $float_value_of_var; // 122.34343
echo "<br/>";
if(empty($var)){
    echo '$var is empty';
} else{
    echo '$var is not empty';
}
echo "<br/>";
if(is_array($var)){
    echo '$var is an array';
} else{
    echo '$var is not an array';
}
echo "<br/>";
$stringserialize = serialize($var);
echo $stringserialize;
echo "<br/>";
$unserializestring = unserialize($stringserialize);
var_dump  ($unserializestring);
echo "<br/>";
echo "<br/>";
echo var_export($var);
echo "<br/>";
echo "<br/>";
echo gettype($var);
echo "<br/>";
echo "<br/>";
$myValue = 0.2;
var_dump(boolval($myValue));
echo "<br/>";
echo "<br/>";
var_dump(boolval(new StdClass));
unset($var);
var_dump($var);
?>