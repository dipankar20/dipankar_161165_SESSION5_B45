<?php
    //boolean example start here
    $decision = true;
    if($decision){
        echo "The decision is True!";
    }
    $decision = false;
    if($decision){
        echo "The decision is False!";
    }
    //boolean example end here

    //Integer & Float example start here
    $value1 = 100; //integer
    $value2 = 55.35; //float
    //Integer & Float example end here

    //String example start here
     $myString1 = 'abcd1234#$% $value1';
     $myString2 = "abcd1234#$% $value1";
     echo "<br/>";
     echo $myString1."<br/>".$myString2."<br/>";
    $heredocString =<<<BITM
        heredoc line1 $value1<br/>
        heredoc line2 $value1<br/>
        heredoc line3 $value1<br/>
BITM;
    echo $heredocString;
    $newdocString =<<<'BITM'
        heredoc line1 $value1<br/>
        heredoc line2 $value1<br/>
        heredoc line3 $value1<br/>
BITM;
    echo $newdocString;
    //String example end here

    //Array example start here
    $indexedArray = array(1,2,3,4,5,6,7,8);
    print_r ($indexedArray);

    $indexedArray2 = array("Toyota","BMW",3,5.17,"Jaguar","Nissan","Ford");
    print_r ($indexedArray2);

    $ageArray = array("Rahim"=>23, "Moynar Ma"=>57, "Kuddus"=>35, "Abul"=>36);
    print_r($ageArray);
    $ageArray['Moynar Ma'] = 34;
    echo ($ageArray['Moynar Ma']);
    echo "<pre>";
        print_r($ageArray);
    echo "</pre>";
    //Array example end here
?>